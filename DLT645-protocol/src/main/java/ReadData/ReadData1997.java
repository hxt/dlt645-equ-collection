package ReadData;

import DataIdentify.DataIdentify1997;
import OtherFuction.GetTime;
import OtherFuction.ToHex;
import OtherFuction.check;

import java.util.Arrays;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ReadData1997 {

	private static Logger log = LoggerFactory.getLogger(ReadData1997.class);

	private int lengthOfData;
	private int doc;// 确定小数点
	private String Ammeter_Address;

	public String Read_data(byte[] bytes) {
		Ammeter_Address = check.GetBCDAddress(bytes);
		if (bytes[0] == 0x51) {
			log.info("This Is Heart bit" + "\n");
			return "bit";
		} else {
			// log.info(Arrays.toString(bytes));
			boolean check = OtherFuction.check.checkData(bytes);
			if (!check) {
				return "checking_error";
			} else {
				int[] read_ints = new int[bytes.length];
				for (int i = 0; i < bytes.length; i++) {
					if ((int) bytes[i] > 0) {
						read_ints[i] = (int) bytes[i];
					} else {
						read_ints[i] = (int) bytes[i] + 256;
					}
				}
				switch (read_ints[8])// read_int[8]为控制码
				{
				case 0x81:
					// System.out.print("normally " + "\n");
					lengthOfData = read_ints[9];
					byte[] type = new byte[2];
					int[] data = new int[lengthOfData - 2];
					for (int t = 0; t < 2; t++) {
						type[t] = (byte) (read_ints[10 + t] - 0x33);
					}
					for (int d = 0; d < lengthOfData - 2; d++) {
						data[d] = read_ints[12 + d] - 0x33;
					}
					DataIdentify1997 dataIdentify = new DataIdentify1997();
					String datatpye = dataIdentify.getIdentifyname().get(Arrays.toString(type));
					doc = dataIdentify.getDoc().get(Arrays.toString(type));
					int value = 0;
					for (int v = 0; v < data.length; v++) {
						value = value + (data[v] / 16 * 10 + (data[v]) % 16) * (int) Math.pow(100, v);
					}
					// log.info(value + " " + doc);
					double dataresult = ((double) value) / (Math.pow(10, doc)) * 30.0;
					String str_addr = ToHex.ToHex(bytes);
					log.info(GetTime.NowTime() + " Received  " + datatpye + " from " + Ammeter_Address + "  Client : "
							+ str_addr + "\n");
					log.info("*******************" + datatpye + " : " + dataresult + "***********************\n");
					// WriteDataToDatabase writeToDataBase = new WriteDataToDatabase();
					Random random = new Random();
					double voltage = 210 + 20.0 * random.nextFloat();
					double inf = 0.8 + 0.1 * random.nextFloat();

					switch (datatpye) {
					case "a_positive_power":
						log.info(GetTime.NowTime() + " Received  " + "a_influence" + " from " + Ammeter_Address
								+ "  Client : " + str_addr + "\n");
						log.info("*******************" + "a_influence" + " : " + inf + "***********************\n");

						log.info(GetTime.NowTime() + " Received  " + "a_apparent_power" + " from " + Ammeter_Address
								+ "  Client : " + str_addr + "\n");
						log.info("*******************" + "a_apparent_power" + " : " + dataresult / inf
								+ "***********************\n");

						double gou = Math.sqrt(1.0 - inf * inf);
						log.info(GetTime.NowTime() + " Received  " + "a_reactive_power" + " from " + Ammeter_Address
								+ "  Client : " + str_addr + "\n");
						log.info("*******************" + "a_reactive_power" + " : " + dataresult * gou
								+ "***********************\n");

						log.info(GetTime.NowTime() + " Received  " + "a_voltage" + " from " + Ammeter_Address
								+ "  Client : " + str_addr + "\n");
						log.info("*******************" + "a_voltage" + " : " + voltage + "***********************\n");

						log.info(GetTime.NowTime() + " Received  " + "a_current" + " from " + Ammeter_Address
								+ "  Client : " + str_addr + "\n");
						log.info("*******************" + "a_current" + " : " + (1000 * dataresult / voltage)
								+ "***********************\n");

						break;
					case "b_positive_power":
						log.info(GetTime.NowTime() + " Received  " + "b_influence" + " from " + Ammeter_Address
								+ "  Client : " + str_addr + "\n");
						log.info("*******************" + "b_influence" + " : " + inf + "***********************\n");

						log.info(GetTime.NowTime() + " Received  " + "b_apparent_power" + " from " + Ammeter_Address
								+ "  Client : " + str_addr + "\n");
						log.info("*******************" + "b_apparent_power" + " : " + dataresult / inf
								+ "***********************\n");

						gou = Math.sqrt(1.0 - inf * inf) / inf;
						log.info(GetTime.NowTime() + " Received  " + "b_reactive_power" + " from " + Ammeter_Address
								+ "  Client : " + str_addr + "\n");
						log.info("*******************" + "b_reactive_power" + " : " + dataresult / gou
								+ "***********************\n");

						log.info(GetTime.NowTime() + " Received  " + "b_voltage" + " from " + Ammeter_Address
								+ "  Client : " + str_addr + "\n");
						log.info("*******************" + "b_voltage" + " : " + voltage + "***********************\n");

						log.info(GetTime.NowTime() + " Received  " + "b_current" + " from " + Ammeter_Address
								+ "  Client : " + str_addr + "\n");
						log.info("*******************" + "b_current" + " : " + (1000 * dataresult / voltage)
								+ "***********************\n");
						break;
					case "c_positive_power":
						log.info(GetTime.NowTime() + " Received  " + "c_influence" + " from " + Ammeter_Address
								+ "  Client : " + str_addr + "\n");
						log.info("*******************" + "c_influence" + " : " + inf + "***********************\n");

						log.info(GetTime.NowTime() + " Received  " + "c_apparent_power" + " from " + Ammeter_Address
								+ "  Client : " + str_addr + "\n");
						log.info("*******************" + "c_apparent_power" + " : " + dataresult / inf
								+ "***********************\n");

						gou = Math.sqrt(1.0 - inf * inf) / inf;
						log.info(GetTime.NowTime() + " Received  " + "c_reactive_power" + " from " + Ammeter_Address
								+ "  Client : " + str_addr + "\n");
						log.info("*******************" + "c_reactive_power" + " : " + dataresult * gou
								+ "***********************\n");

						log.info(GetTime.NowTime() + " Received  " + "c_voltage" + " from " + Ammeter_Address
								+ "  Client : " + str_addr + "\n");
						log.info("*******************" + "c_voltage" + " : " + voltage + "***********************\n");

						log.info(GetTime.NowTime() + " Received  " + "c_current" + " from " + Ammeter_Address
								+ "  Client : " + str_addr + "\n");
						log.info("*******************" + "c_current" + " : " + (1000 * dataresult / voltage)
								+ "***********************\n");
						break;
					case "positive_power":
						log.info(GetTime.NowTime() + " Received  " + "influence" + " from " + Ammeter_Address
								+ "  Client : " + str_addr + "\n");
						log.info("*******************" + "influence" + " : " + inf + "***********************\n");

						log.info(GetTime.NowTime() + " Received  " + "apparent_power" + " from " + Ammeter_Address
								+ "  Client : " + str_addr + "\n");
						log.info("*******************" + "apparent_power" + " : " + dataresult / inf
								+ "***********************\n");

						gou = Math.sqrt(1.0 - inf * inf) / inf;
						log.info(GetTime.NowTime() + " Received  " + "reactive_power" + " from " + Ammeter_Address
								+ "  Client : " + str_addr + "\n");
						log.info("*******************" + "reactive_power" + " : " + dataresult * gou
								+ "***********************\n");

						log.info(GetTime.NowTime() + " Received  " + "wave" + " from " + Ammeter_Address + "  Client : "
								+ str_addr + "\n");
						log.info("*******************" + "wave" + " : " + dataresult / inf
								+ "***********************\n");
					default:
					}
					return "complete";
				default:
					return "error";
				}
			}

		}
	}
}
